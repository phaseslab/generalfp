# FPClasses.py contains the definitions for the VASPjob, INCAR,POSCAR,
# KPOINT and subjob classes
import os 
from copy import deepcopy

class ClassPOTCAR():
  # The POTCAR class doesn't have many features at the moment.
  def __init__(self,path):
    # A POTCAR is implemented as the lines from a POTCAR file.
    # no check is made to ensure the file at path is a POTCAR file.
    try:
      f = file(path)
      self.potential = f.read()
      f.close()
    except IOError as e:
      print("IOError: is there a file at {}?".format(path))

  def __str__(self):
    # A potcar is just a string at the moment. return it.
    return self.potential
  
  def __eq__(self,other):
    #compare POTCARS by looking at strings
    return str(self)==str(other)

  def update(self,filename):
    # create a copy of this POTCAR object, then change its contents.
    # This is basically the same as just creating a new object right 
    # now, but may be updated in the future depending on how the 
    # POTCAR implementation changes
    newObject = deepcopy(self)

    # make sure the update file actually exists
    try:
      f = file(filename)
      newObject.potential = f.read()
      f.close()
      
    # if it didn't, ignore the failure and warn the user
    except IOError:
      #print "Attempted to update POTCAR. {} not found. Continuing without update".format(filename)
      return newObject
      
    except Exception as e:
      print("Non-I/O exception ocurred in ClassPOTCAR.update(\"{}\")".format(filename))
      raise e
    return newObject

  def writeOut(self,path):
    f = open(path+"/POTCAR",'w')
    f.write(str(self))
    f.close()
  

class ClassPBSScript():
  # The PBS script class is not feature rich at the moment
  # it's just a string containing the script to submit.

  def __init__(self,path):
    # a PBS script is implemented as just the lines from reading a
    # file at path
    try:
      f = file(path)
      self.script = f.read()
      self.name = path
      f.close()
      # generalFP needs a token to show that a job is finished
      # deleting and then creating a '.jobComplete' will accomplish
      # that goal
      
      if not("touch .jobComplete" in self.script):
        self.script += "\ntouch .jobComplete"
    except IOError as e:
      print("IOError initializing a PBSScript object. Is there a PBS file at {}?".format(path))
      

  def __str__(self):
    return self.script
  
  def __eq__(self,other):
    return str(self)==str(other)
  
  def getName(self):
    return self.name

  def update(self,filename):
    # create a copy of this PBSScript object then change its contents.
    # This is basically the same as just creating a new object right 
    # now, but may be updated in the future depending on how the PBS 
    # script implementation changes
    newObject = deepcopy(self)

    # make sure the update source exists
    try:
      f = file(filename)
      newObject.script = f.read()
      f.close()

    # if the source didn't exist, ignore the failure and warn the user
    except IOError as e:
      #print "Attempted to update PBS script. {} not found. Continuing without update".format(filename)
      return newObject
    except Exception as e:
      print("Non-I/O exception ocurred in ClassPBSScript.update(\"{}\")".format(filename))
      raise e

    return newObject

  def writeOut(self,path):
    f = open(path+"/"+self.getName(),'w')
    f.write(str(self))
    f.close()

class ClassKPOINTS():
  #KPOINT class is also not feature rich currently.
  #KPOINT class contains a string made up of the KPOINT file it read.
  def __init__(self,path):
    f = file(path)
    self.KPOINTS = f.read()
    f.close()

  def __str__(self):
    return self.KPOINTS
  
  def __eq__(self,other):
    return str(self)==str(other)

  def update(self,filename):
    # create a copy of this KPOINT object, then change its contents.
    # This is basically the same as just creating a new object right 
    # now, but may be updated in the future depending on how the 
    # KPOINT script implementation changes
    newObject = deepcopy(self)
    
    #make sure the update source exists
    try:
      f = file(filename)
      newObject.KPOINTS = f.read()
      f.close()

    # if the source didn't exist, ignore the failure and warn the user
    except IOError:
      #print "Attempted to update KPOINTS. {} not found. Continuing without update".format(filename)
      return newObject
    except Exception as e:
      print("Non-I/O exception ocurred in ClassKPOINTS.update(\"{}\")".format(filename))
      raise e

    return newObject

  def writeOut(self,path):
    f = open(path+"/KPOINTS",'w')
    f.write(str(self))
    f.close()

class ClassPOSCAR():
  def __init__(self,posPath):
    # Open the POSCAR
    #self.scale = [1, 1, 1]
    try:
      posFile = file(posPath)
      # Read title, scale, and lattice vectors
      self.title = posFile.readline().strip()
      self.scale = float(posFile.readline().strip())
      self.a = [float(x) for x in posFile.readline().strip().split()]
      self.b = [float(x) for x in posFile.readline().strip().split()]
      self.c = [float(x) for x in posFile.readline().strip().split()]
    # vasp will sometimes have a string declaring the order of 
    # elements in the list of atomic positions
      self.strings = []
    # The number of atomic positions for the nth potential in POTCAR
      self.atomNums = []
    # The positions of all atoms
      self.atomPos = []

    # read through strings until something can be read as an int
      proceed = True    
      while proceed:
        line = [x.strip() for x in posFile.readline().split()]
        try:
          int(line[0])
          proceed = False
        # store the first line with an integer in atomNums
          self.atomNums=[int(x) for x in line]
        except:
          # store non-integer lines in self.strings
          self.strings.append(line)

    # Read out the remaining lines as atomic positions
      self.coordType= posFile.readline()
      #print "Coordtype in POSCAR class:",self.coordType

      proceed = True
      for line in posFile:
        #print line
        splitLine = line.split()
        newLine=[float(x.strip()) for x in splitLine[0:3]]+splitLine[4:]
        #print newLine
        if newLine!=[]:
          self.atomPos.append(newLine)  



    #close the poscar file now that it's been read
      posFile.close()
    except IOError as e:
      raise e

  def __str__(self):
    outArray=[]
    outArray.append(self.title)
    outArray.append(str(self.scale))
    outArray.append(" ".join([str(el) for el in self.a]))
    outArray.append(" ".join([str(el) for el in self.b]))
    outArray.append(" ".join([str(el) for el in self.c]))
    for el in self.strings:
      outArray.append(" ".join(el))

    outArray.append(" ".join([str(el) for el in self.atomNums]))
    outArray.append(self.coordType)
    for el in self.atomPos:
      outArray.append(" ".join([str(x) for x in el]))
      
    return "\n".join(outArray)+"\n\n"

  def __eq__(self,other):
    #print "self:\n", self
    #print "other:\n",other
    return str(self)==str(other)

  def writeOut(self,path,scale=None):
    if scale!=None:
      self.scale = scale

    f = open(path+"/POSCAR",'w')
    f.write(str(self))
    f.close()

  def setScale(self,val):
    self.scale = val

  def getScale(self):
    return self.scale



class ClassINCAR:
    # INCAR is a class for representing INCAR files internally
    # INCARs will be lists of pairs of flags and values
  
  def __init__(self,fileName):
    #an INCAR should simply be a list of flags and values
    #open the file and read
    try:
      f = open(fileName,'r')
    # construct the list of flag value pairs by splitting the lines by
    # =, stripping the remainder so that there's no whitespace, and 
    # making a pair out of that result
      lines = f.readlines()

      flags = [line.split("=") for line in lines if line.strip()!='']
    
      self.flagDict = {flagPair[0].strip():flagPair[1].strip() for flagPair in flags}

    except IOError as e:
      raise e

  def update(self,fileName):
    # update creates a new copy of this INCAR object, and modifies the
    # new object's dictionary to match contents of fileName
    newObject = deepcopy(self)

    # make sure the update source actually exists
    try:
      f = open(fileName,'r')
      tempLines = [el.strip().split("=") for el in f.readlines() if el.strip()!='']
      newDict = {el[0].strip():el[1].strip() for el in tempLines}
      for el in newDict:
        newObject.flagDict[el]=newDict[el]

    # if the source didn't exist, ignore the failure and warn the user
    except IOError:
      #print "Attempted to update INCAR. {} not found. Continuing without update".format(fileName)
      return newObject
    except Exception as e:
      print("Non-I/O exception ocurred in INCAR.update(\"{}\")".format(fileName))
      raise e
    return newObject
    

  def __eq__(self,other):
    return self.flagDict==other.flagDict
  
  def __str__(self):
    # Join dictionary elements by = signs. join those with \newline
    output = "\n".join(sorted([" = ".join([el,self.flagDict[el]]) for el in self.flagDict]))
    return output

  def writeOut(self,path):
    # open a path and write
    f = open(os.path.join(path,"INCAR"),'w')
    f.write(str(self))
    f.close()


  def setFlags(self,updateFlagDict):
    for entry in updateFlagDict:
      # print "original: ",self.flagDict[entry]
      # print "new:      ",updateFlagDict[entry]
      self.flagDict[entry] = updateFlagDict[entry]

  def setPositionRelax(self):
    # relax atomic positions
    self.flagDict['ISIF']='2'

  def setCellShapeRelax(self):
    # relax only cell shape
    self.flagDict['ISIF']='5'

  def setFullRelax(self):
    # relax cell shape and atomic positions
    self.flagDict['ISIF']='4'

  def setStatic(self):
    # switch to static calculation mode
    self.flagDict['NSW']='0'
    self.flagDict['ISIF']='2'


class VASPjob():  
  def __init__(self,path,INCAR,POSCAR,KPOINTS,POTCAR,PBSScript):
  #VASPjob is a class containing everything necessary to run vasp jobs
  # a VASPjob contains:
  #      an object of INCAR class (currently dictionary based)
  #      an object of POSCAR class (currently mostly lists)
  #      a path to a KPOINT file
  #      a path to a pbs submission script
    self.path      = path
    self.INCAR     = INCAR
    self.POSCAR    = POSCAR
    self.KPOINTS   = KPOINTS
    self.POTCAR    = POTCAR
    self.PBSScript = PBSScript

  @classmethod
  def newFromPath(cls,path):
    directoryContents = os.listdir(path)
    pbsFiles     = [el for el in directoryContents if 'pbs'     in el]
    pathPBSString = ''
    for el in pbsFiles: 
      if el.endswith('pbs'): 
        pathPBSString=el
    
    INCAR = ClassINCAR(os.path.join(path,"INCAR"))
    POSCAR = ClassPOSCAR(os.path.join(path,"POSCAR"))
    KPOINTS = ClassKPOINTS(os.path.join(path,"KPOINTS"))
    POTCAR = ClassPOTCAR(os.path.join(path,"POTCAR"))
    PBSScript = ClassPBSScript(os.path.join(path,pathPBSString))
    return VASPjob(path,INCAR,POSCAR,KPOINTS,POTCAR,PBSScript)
    

  def __eq__(self,otherJob):
    samePath    = self.path==otherJob.path
    #if not(samePath): print "same path:", samePath

    sameINCAR   = self.INCAR==otherJob.INCAR
    #if not(samePath): print "same path:", samePath

    samePOSCAR  = self.POSCAR == otherJob.POSCAR
    #if not(samePOSCAR): print "same POSCAR:", samePOSCAR

    sameKPOINTS = self.KPOINTS == otherJob.KPOINTS
    #if not(sameKPOINTS): print "same KPOINTS:", sameKPOINTS

    sameScript  = self.PBSScript == otherJob.PBSScript
    #if not(sameScript): print "same script:", sameScript
    
    return samePath&sameINCAR&samePOSCAR&sameKPOINTS&sameScript


  def setPath(self,newPath):
    # setPath changes a VASPjob's path to newPath
    self.path = newPath

  def getPath(self):
    return self.path

  def setup(self):
    information = """VASPjob's setup method sets up a job when called by removing the .jobComplete file, and writing the contents of the VASPjob to the destination folder."""
    ## This old code checked first, to see if setting up the 
    # calculation was necessary. This functionality has been moved to
    # other parts of the code. At time of commenting, it was part of 
    # tendJobList, in FPUtil.py

    # setup a file by writing to the path
    # if os.path.isdir(self.path):
    #   print "Directory exists at {}".format(self.path)
    #   try:
    #     # since the path already existed, a job at least started here
    #     # make a new job out of the inputs there
    #     oldJob = VASPjob.newFromPath(self.path)
    #     if oldJob==self:
    #       # compare the old job to this one, update input if necessary
    #       try:
            
    #         f = open(os.path.join(self.path,"OUTCAR"))
    #         for line in f:
    #           if "required" in line:
    #             print "  Previous job with these inputs converged."
    #             print "  No job was created"
    #             # We don't need to overwrite files
    #             return False
            
    #       except Exception as e:
    #         # Catch any exception coming from trying to open the old job's OUTCAR
    #         print "  Inputs exist, and were up to date, but OUTCAR didn't exist. Creating new job."
    #     else:
    #       print "  The old job was not the same as the new job"
    #   except Exception as e:
    #     print "  Previous VASPjob was incomplete. Creating new files."
    # else:
    #   print "  no directory exists at {}. Creating that directory".format(self.path)
    #   os.makedirs(self.path)

    # since we're setting up the job, remove the old .jobComplete if it exists
    if os.path.isfile(os.path.join(self.path,".jobComplete")):
      os.remove(os.path.join(self.path,".jobComplete"))
    # write all the new files
    self.INCAR.writeOut(self.path)
    self.POSCAR.writeOut(self.path)
    self.KPOINTS.writeOut(self.path)
    self.POTCAR.writeOut(self.path)
    self.PBSScript.writeOut(self.path)
    #files are written, and a new run must be performed
    return True
    
    

  def run(self):
    runSuccess= False
    cwd= os.getcwd()
    try:
      os.chdir(self.path)
      os.system("qsub "+self.PBSScript.getName())
      runSuccess=True
    finally:
      os.chdir(cwd)
      return runSuccess

  def localRun(self):
    runSuccess= False
    cwd= os.getcwd()
    try:
      os.chdir(self.path)
      os.system("vasp")
      runSuccess=True
      f = open('.jobComplete','w')
      f.write('')
    finally:
      os.chdir(cwd)
      return runSuccess

  def isFinished(self):
    #isFinished is a method for a vasp job that will check if the job 
    # is finished or not. 
    #Since all of the PBS scripts remove, and then create a 
    # .jobComplete file, we can just look for that file.
    return os.path.isfile(os.path.join(self.getPath(),".jobComplete"))


  def isConverged(self,jobType=None):
    # the isConverged method looks at the OUTCAR from a vasp job and
    # checks if that OUTCAR contains the string "required" which 
    # designates a job that has reached the accuracy required to stop
    # vasp's structural energy minimization routine.
    
    #this behavior is modified for 'static' jobs, which aren't 
    # structurally modified.
    if str(jobType).lower() == 'static':
      return True
    
    try:
        
      f = open(os.path.join(self.path,"OUTCAR"))
      for line in f:
        if "required" in line:
          #print "  Previous job with these inputs converged."
          # We don't need to overwrite files
          return True
        
    except IOError as e:
      # Catch any exception coming from trying to open the old job's OUTCAR
      print("  Error checking for output in OUTCAR. Does OUTCAR exist?")
    except Exception as e:
      print("  Non I/O exception thrown in FPClasses.py's VASPjob class's method isConverged")
    return False
