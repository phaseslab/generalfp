#!/usr/bin/env python
from numpy import exp
from scipy.integrate import quad
# this code calculates debye functions of order n, for use in debye models of 
# finite temperature thermodynamic behavior of crystals

def debyeFunc(ratio,n=3):
    """
    debyeFunc returns the value of the nth order debye function evaluated
      for a ratio T_Debye/T
    """
    kernel = lambda t: (t**n)/(exp(t)-1)
    debVal = 0
    if ratio>1e-10:
        quadValue, errorEst = quad(kernel,0,min(ratio,30)) 
        # since integral(kernel,30,infinity)<10^-8, truncating at 30 leads to
        #  an error of <10^-8, and a relative error of <10^-9, so even though
        #  this function is often multiplied by large values, the relative 
        #  error reamins small

        # scipy.integrate.quad has a builtin error estimation
        # print("Error estimate in debye integration: {}".format(errorEst))
        debVal= quadValue*n/ratio**n
    else:
        # ratio is small (<10**-10), so we can integrate the first order taylor
        #  expansion this gives (ratio**n)/n, so the limit of debVal is
        #  n/(ratio**n) * (ratio**n)/n, which is just 1.
        debVal = 1

    return debVal

def debyeCV(ratio):
    """
    debyeCV returns the unitless heat capacity for a ratio of T_Debye/T
    """
    kernel = lambda t: ((t**4)*exp(t))/((exp(t)-1)**2)
    debCV = 0
    if ratio>1e-10:
        quadValue, errorEst = quad(kernel,0,min(ratio,30)) 
        # since integral(kernel,30,infinity)<10^-7, truncating at 30 leads to
        #  an error of <10^-7, and a relative error of <10^-9, so even though
        #  this function is often multiplied by large values, the relative 
        #  error reamins small

        # scipy.integrate.quad has a builtin error estimation
        # print("Error estimate in debye integration: {}".format(errorEst))
        debCV= (9*quadValue)/ratio**3
    else:
        # ratio is small (<10**-10), so we can integrate the first order taylor
        #  expansion this gives (ratio**3)/3, so the limit of debyeCV is
        #  n/(ratio**n) * (ratio**n)/n, which is just 1.
        debCV = 1

    return debCV




