import os
import sys
import subprocess
from time import sleep
from numpy import linalg

arguments = sys.argv
# set up executable path, name, and the library path
executablePath= arguments[0]
executableName = executablePath.split("/")[-1]
libPath = executablePath.split("/")[:-2]
libPath.extend(['lib',''])
libPath = "/".join(libPath)

# add the generalFP library path to sys.path
sys.path.append(libPath)
from FPClasses import *

def volumeFromPath(path,fileType='CONTCAR'):
    # inputs: path and (optionally) fileType
    # output: volume of the file at path in angstrom^3
    #
    # given a path, volumeFromPath goes to that path, tries to open a
    # contcar file at that path, and then returns the volume of that
    # contcar. Contcar is the default type of file to open, since 
    # the intention for this routine is mostly to get volumes from
    # finished jobs
    
    # Create the path and try to open it
    CONTCARpath = os.path.join(path,fileType)
    try:
        f=open(CONTCARpath,'r')
        fileLines = f.readlines()
    except IOError as e:
        print('Error opening CONTCAR at {}. Does it exist?'.format(CONTCARpath))
        raise e

    # in a contcar, there are only 4 lines related to the cell volume
    volumeLines = [el.strip() for el in fileLines[1:5]]

    # the scale is the first of those
    scale = float(volumeLines[0])
    # the lattice description is the following three lines
    lattice = [[float(el) for el in line.split()] for line in volumeLines[1:4]]

    # volume of a parallelipiped is det(spanning vectors)
    volume = scale**3 * linalg.det(lattice)
    return volume

def energyFromPath(path):
    # inputs: path to a directory with an OSZICAR
    # output: calculated energy in the OSZICAR. units of eV
    # To find the energy of a finished calculation at a path, open
    # the OSZICAR a job should create there when it finishes

    # try to open OSZICAR and read energy relevant lines
    OSZICARpath = os.path.join(path,'OSZICAR')
    try:
        f = open(OSZICARpath,'r')
        energyLines = [line for line in f if 'E0=' in line]
    except IOError as e:
        print('Error opening OSZICAR at {}. Does an OSZICAR exist?'.format(OSZICARpath))
        raise e

    # the last enegy line is the preferred one
    energyLine = energyLines[-1]

    # string parsing to get the value, assuming a typical VASP output
    energyLine = energyLine.split('=')
    energy = energyLine[2].split()
    return float(energy[0])
    
def pressureFromPath(path):
    # inputs: path to a directory with an OUTCAR
    # output: calculated pressure in the OUTCAR. units of kilobar
    # To find the pressure of a finished calculation at a path, open
    # the OUTCAR created by a completed VASP job

    # try to open OUTCAR and read pressure relevant lines
    OUTCARpath = os.path.join(path,'OUTCAR')
    try:
        f = open(OUTCARpath,'r')
        pressureLines = [line for line in f if 'pressure' in line]
    except IOError as e:
        print('Error opening OUTCAR at {}. Does an OUTCAR exist?'.format(OUTCARpath))
        raise e

    # the last enegy line is the preferred one
    pressureLine = pressureLines[-1]

    # string parsing to get the value, assuming a typical VASP output
    pressureLine = pressureLine.split('=')
    pressure = pressureLine[1].split()
    return float(pressure[0])    

def tendJobList(jobListIn,jobType=None,waitTime=20, local=False):
    #tendJobList is a function that will submit and look after a
    runningJobs = []
    # make a copy of the input job list to work on
    jobList = jobListIn[:]


    information = """VASPjob's setup method should only set up a job if a new job needs to be run. A job needs to be run if there\'s nothing at the job\'s path, if the job previously set up at that path is incomplete, or if the new job is different than the old job. The only way we don't need to run a job, is if the folder is already there, the job is complete, and the job has converged. """
    #try to set up all the jobs, run them, and add them to the list
    # of running jobs 
    for job in jobList:

        # Check on the directory
        dirExists = os.path.isdir(job.getPath())
        sameJob = False
        jobConverged = False
        if dirExists:
            try:
                # since the path already existed, a job at least started here
                # make a new job out of the inputs there
                oldJob = VASPjob.newFromPath(job.getPath())
                sameJob = (oldJob==job)
                if sameJob:
                    jobConverged = job.isConverged(jobType=jobType)
              # compare the old job to this one, update input if necessary
            except IOError as e:
                print("  Previous VASPjob was incomplete. Creating new files.")
            except Exception as e:
                print("  A non-IO error occurred in tendJobList while trying to set up a job")
                print(e)
                print()
        else:
            print("  no directory exists at {}. Creating that directory".format(job.path))
#        if os.path.isdir(job.path):
            os.makedirs(job.path)

        # if the job directory doesn't exist, the job is new, and 
        # the job hasn't converged, setup the new job.
        if (not(dirExists) or not(sameJob) or not(jobConverged)):
            if job.setup():
                if local == True:
                    job.localRun()
                else:
                    job.run()
                runningJobs.append(job)
        else:
            # everything was okay, so we don't need to add this job.
            print("  VASP job already existed and converged at {}/ with these inputs. Skipping redundant job".format(job.getPath()))
                
    finishedJobs = []
    while len(runningJobs)!=0:
        # while the runningJobs still exists
        for job in runningJobs[:]:
            # check on jobs. remove them if they're done
            if job.isFinished():
                runningJobs.remove(job)
                finishedJobs.append(job)
        if len(runningJobs)!=0:
            print("\n",len(runningJobs),"jobs remaining\n")
            sleep(waitTime)

    for i in range(10):
        if len(finishedJobs)==0:
            break
        print("\nfinished jobs:")
        for job in finishedJobs:
            print(job.getPath())
            if job.isConverged(jobType=jobType):
                finishedJobs.remove(job)
        if len(finishedJobs)>0:
            print("{} jobs finished, but not converged after loop {}. Waiting {} seconds".format(len(finishedJobs),i+1,waitTime))
            sleep(waitTime)
            

        
    

# workDir = os.getcwd() #get the current working directory
# username = os.environ['USER']

# generalFPDir = '/gpfs/home/'+username+'/git/generalfp' 
# #This will change for different users, and will likely require an installation script
# scriptLocation = "scripts"

# def getEnergy(path):
#   p = subprocess.Popen("grep E0 OSZICAR | tail -1",
#                        stdin=subprocess.PIPE,
#                        stdout=subprocess.PIPE,
#                        close_fds=True,
#                        cwd=path,
#                        shell=True)
#   stdout_text, stderr_text = p.communicate()
#   temp = stdout_text.split("=")
#   #format of OSZICAR file assumed to be F=# E0=# d E=# other things
#   energy = float(temp[2].replace('d E',''))
#   return energy




# def getJobNumber(inString):
#   #getJobNumber takes the output from qsub (like $(jobnumber).cyberstar.psu.edu)
#   # it outputs just the job number
#   tempList = inString.strip().split('.')
#   return tempList[0]
  

# def findSpaceGroup(path):
#   #define the path to the command to execute
#   execFolder = path
#   fSGname = os.path.join(generalFPDir,scriptLocation,"find_spacegroup")

#   #go execute the command and get the output
#   p = subprocess.Popen(fSGname,
#                        stdin=subprocess.PIPE,
#                        stdout=subprocess.PIPE,
#                        close_fds=True,
#                        cwd=os.path.join(workDir,execFolder),
#                        shell=True)

#   stdout_text, stderr_text =  p.communicate()

#   #split the output into lines
#   outLines = stdout_text.split("\n")

#   # concatenate all lines with the phrase space group as the output
#   out = ''
#   for line in outLines:
#     if line.find("Space group")!= -1:
#       out += line

#   # warning message for if there is no output
#   if out == '':
#     print "WARNING: find_spacegroup script failed in folder: " + path
#     print "check your POSCAR and make sure find_spacegroup is at "
#     print fSGname
#     print "note: make sure your POSCAR has a line after the last atom position"
#     print "as of fropho v1.2.0.2, this will crash find_spacegroup(which depends on fropho)"

#   return out 

# def calculateScales(fileScales,scalePaths,stepPath,INCAR,POSCAR,outArray,precision):
#   print "  Calculating new scales"
#   jobList = []
#   for scale in fileScales:
#     # make a directory for each scale and fill the new directory with vasp files
#     scaleName = ("vol_"+str(scale))
#     scalePaths.append(os.path.join(stepPath,scaleName))
    
#     createLowJob(scalePaths[-1],scale,INCAR,POSCAR,precision)

#     #print "not removing contcar to avoid recalculating during testing"
#     if os.path.exists(os.path.join(scalePaths[-1],'CONTCAR')):
#       p = subprocess.Popen("rm CONTCAR",
#                            stdin=subprocess.PIPE,
#                            stdout=subprocess.PIPE,
#                            close_fds=True,
#                            cwd=scalePaths[-1],
#                            shell=True)

#       stdout_text, stderr_text = p.communicate()


#     jobList.append((scalePaths[-1],scale,submitJob(scalePaths[-1])))

#     # during testing, it may be desirable to quickly delete all jobs out of the queue
#     #print "deleting submitted job, since I'm just testing at the moment"
#     #subprocess.call("qdel "+jobList[-1][2],shell=True)

#   while jobList:
#     # while there are still jobs running
#     for job in jobList:
#       # look through all the jobs
      
#       # check if a job has finished, by looking for a contcar with
#       # something in it
#       if (os.path.exists(os.path.join(job[0],"CONTCAR"))):
#         if (os.path.getsize(os.path.join(job[0],"CONTCAR"))>0):      # if the job has finished, 
#           try:
#             getEnergy(job[0])
#           except:
#             print "Error in reading energy from scale:",job[1]
#           else:  
#             outArray.append((job[1],getEnergy(job[0])))      # store its scale and energy data
#             jobList.remove(job)      # remove it from jobList

            
#     sleep(1)
#     #print "jobList is",str(len(jobList)),"long"

# def balanceCalculations(fileScales,scalePaths,stepPath,INCAR,POSCAR,outArray,precision):
#   # find out if we need to set more volumes, and calculate new volumes until we don't
#   while True:
#     #sort the no relax output by scale
#     outArray.sort()
    
#     #find the minimum energy, by finding the min using the second element
#     minOut = min(outArray,key=lambda x:x[1])
    
#     minDex = outArray.index(minOut)

#     if minDex==0:
#       #calculate 4 new volumes less than lowest scale
#       newScales = []
#       for i in range(4):
#         newScales.append(minOut[0]-.01*(i+1))
#       calculateScales(newScales,scalePaths,stepPath,INCAR,POSCAR,outArray,precision)
        
#     elif minDex == len(outArray)-1:
#       #calculate 4 new volumes larger
#       newScales = []
#       for i in range(4):
#         newScales.append(minOut[0]+.01*(i+1))

#       calculateScales(newScales,scalePaths,stepPath,INCAR,POSCAR,outArray,precision)
#     elif minDex<3:
#       #calculate 2 new volumes lower than the lowest
#       newScales = []
#       minScale = outArray[0][0]
#       for i in range(2):
#         newScales.append(minScale-.01*(i+1))

#       calculateScales(newScales,scalePaths,stepPath,INCAR,POSCAR,outArray,precision)
#     elif minDex>len(outArray)-4:
#       #calculate 2 new volumes higher than the highest
#       newScales = []
#       maxScale = outArray[-1][0]
#       for i in range(3):
#         newScales.append(maxScale+.01*(i+1))

#       calculateScales(newScales,scalePaths,stepPath,INCAR,POSCAR,outArray,precision)
#     else:
#       break


# def createLowJob(path,scale,INCAR,POSCAR,precision):
#   if not os.path.exists(path):
#     os.mkdir(path) #use the scales to make folders like vol_1.00
    
#   # writing files
#   INCAR.writeOut(path)
#   POSCAR.writeOut(path,scale)
#   if precision=="high":
#     subprocess.call("cp KPOINTS.high "+path+"/KPOINTS",shell=True)
#   else:
#     subprocess.call("cp KPOINTS.low "+path+"/KPOINTS",shell=True)

#   subprocess.call("cp *.q "+path+ "/",shell=True)
#   subprocess.call("cp POTCAR "+path+"/",shell=True)
    
    
# def submitJob(path):
#     # submits a job, and returns an identifier that says what job number this is
#     p = subprocess.Popen("qsub *.q",
#                          stdin=subprocess.PIPE,
#                          stdout=subprocess.PIPE,
#                          close_fds=True,
#                          cwd=path,
#                          shell=True)
#     stdout_text, stderr_text = p.communicate()
#     return getJobNumber(stdout_text)
