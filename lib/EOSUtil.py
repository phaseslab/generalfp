from math import log
from numpy import polyfit,array,poly1d,zeros,fliplr, flipud, matrix
from numpy import r_ as vertcat
from numpy import c_ as horzcat
from numpy.linalg import pinv
# since this code was developed for use on the rcc clusters at psu
# and these clusters have numpy, but don't seem to have scipy,
# I've copied the single routine I want out of scipy
from miniScipy import newton as findRoot

chunit = 1602.189 # chunit is the conversion factor from 
#eV/angstrom^3 to kbar

# For expansions of internal energy in polynomials of strain, there
# Are three major families, eulerian, lagrangian, and logarithmic
eulerianFamily = ['euler','birch-murnaghan','murnaghan','bm']
lagrangianFamily = ['lagrange']
logarithmicFamily = ['hencky','log','logarithmic']
strainFamiles = [eulerianFamily,lagrangianFamily,logarithmicFamily]

def mechanicalPropertiesFromFitCoeff(fitCoeff,
                                     strainType,
                                     degree,
                                     VZeroGuess,
                                     pressureFit=False):
    # fitCoeff are a fitted coefficients
    # strainType and degree tell the function which of a variety of 
    #  methods to use to find mechanical properties
    # VZeroGuess is used as the initial guess for a root finding 
    #  algorithm
    # pressureFit tells you if the coefficients come from a pressure
    #  or an energy fit

    #originally in shunlieos:
    # not supported 1- modified BM 4-parameter
    # not supported 2- modified BM 5-parameter
    #     supported 3- BM 4-parameter
    #     supported 4- BM 5-parameter
    # not supported 5- log 5-parameter
    # not supported 6- log 5-parameter
    if   strainType.lower() in eulerianFamily:
        strainType=strainType
        strain = lambda vol: (vol)**(-2./3.)
        strainDeriv = lambda vol: (-2./3.)*(vol)**(-5./3.)
        
    elif strainType.lower() in lagrangianFamily:
        strainType=strainType
        strain = lambda vol: (vol)**(2./3.)
        strainDeriv = lambda vol: (2./3.)*(vol)**(-1./3.)
        
    elif strainType.lower() in logarithmicFamily:
        strainType=strainType
        strain = lambda vol: log(vol)
        strainDeriv = lambda vol: 1/vol
        
    if pressureFit==True:
        pressurePoly = poly1d(fitCoeff)
        pressureFunction = lambda vol: pressurePoly(strain(vol))
    else:
        energyPoly = poly1d(fitCoeff)
        energyFunction = lambda vol: energyPoly(strain(vol))
        pressureFunction = lambda vol: (-chunit*strainDeriv(vol)*
                                     energyPoly.deriv()(strain(vol)))


    # find equilibrium volume by finding the zero of pressure
    VZero = findRoot(pressureFunction,VZeroGuess)

    if pressureFit ==True:
        if strainType.lower() in eulerianFamily:
            print("degree: {} nCoeff: {}".format(degree,len(fitCoeff)))
            if degree == 3:
                #this is the mechanical properties method for the
                # pressure fit equivalent to BM-4 parameter 
                try:
                    b = fitCoeff[2]
                    c = fitCoeff[1]
                    d = fitCoeff[0]
                except IndexError as err:
                    print("Error: expected 4 coefficients, given {}".format(len(fitCoeff)))
                    raise err

                # The following are analytical expressions for 
                # mechanical properties.
                # I believe they assume a function of the form
                # P = b + c*V^(-2/3)+dV^(-4/3)
                e=0
                P=(8*e)/(3*VZero**(11./3)) + (2*d)/VZero**3 + (4*c)/(3*VZero**(7./3)) + (2*b)/(3*VZero**(5./3))

                B =(2*(44*e + 27*d*VZero**(2./3) + 14*c*VZero**(4./3) + 5*b*VZero**2))/(9*VZero**(11./3));


                BP=((484*e + 243*d*VZero**(2./3) + 98*c*VZero**(4./3) + 25*b*VZero**2)/
                    (132*e +81*d*VZero**(2./3) + 42*c*VZero**(4./3) + 15*b*VZero**2))

                B2P = ((4*VZero**(13./3)*(27*d*(22*e + 7*c*VZero**(4./3) + 10*b*VZero**2) + VZero**(2./3)*(990*b*e*VZero**(2./3) + 7*c*(176*e + 5*b*VZero**2))))/
                       (44*e + 27*d*VZero**(2./3) + 14*c*VZero**(4./3) + 5*b*VZero**2)**3)
                E0 = 0
            elif degree ==4:
                
                #this is the mechanical properties method for the
                # BM 4-parameter fit to energy. 
                try:
                    b = fitCoeff[3]
                    c = fitCoeff[2]
                    d = fitCoeff[1]
                    e = fitCoeff[0]
                    
                except IndexError as err:
                    print("Error: expected 4 coefficients, given {}".format(len(fitCoeff)))
                    raise err

                # The following are analytical expressions for 
                # mechanical properties.
                # I believe they assume a function of the form
                # P = b + c*V^(-2/3)+dV^(-4/3)+eV^(-6/3)
                P=(8*e)/(3*VZero**(11./3)) + (2*d)/VZero**3 + (4*c)/(3*VZero**(7./3)) + (2*b)/(3*VZero**(5./3))

                B =(2*(44*e + 27*d*VZero**(2./3) + 14*c*VZero**(4./3) + 5*b*VZero**2))/(9*VZero**(11./3));


                BP=((484*e + 243*d*VZero**(2./3) + 98*c*VZero**(4./3) + 25*b*VZero**2)/
                    (132*e +81*d*VZero**(2./3) + 42*c*VZero**(4./3) + 15*b*VZero**2))

                B2P = ((4*VZero**(13./3)*(27*d*(22*e + 7*c*VZero**(4./3) + 10*b*VZero**2) + VZero**(2./3)*(990*b*e*VZero**(2./3) + 7*c*(176*e + 5*b*VZero**2))))/
                       (44*e + 27*d*VZero**(2./3) + 14*c*VZero**(4./3) + 5*b*VZero**2)**3)

                E0 = 0

    else:
        if strainType.lower() in eulerianFamily:
            if degree == 3:
                #this is the mechanical properties method for the
                # BM 4-parameter fit to energy. 
                try:
                    a = fitCoeff[3]
                    b = fitCoeff[2]
                    c = fitCoeff[1]
                    d = fitCoeff[0]
                except IndexError as err:
                    print("Error: expected 4 coefficients, given {}".format(len(fitCoeff)))
                    raise err
                
                # the following are analytic equations for a polynomial
                # of the for E = a + bV^(-2/3)+cV^(-4/3)+dV^(-2)
                P=(2*d)/VZero**3 + (4*c)/(3*VZero**(7./3)) + (2*b)/(3*VZero**(5./3))
                P=P*chunit
                
                B =((2*(27*d*VZero**(2./3) + 14*c*VZero**(4./3) + 5*b*VZero**2))/
                    (9*VZero**(11./3)))
                # chunit converts to kBar, chunit/10 converts to GPa
                B = B*(chunit/10)

                # First pressure derivative of bulk modulus
                BP=((243*d*VZero**(2./3) + 98*c*VZero**(4./3) + 25*b*VZero**2)/
                    (81*d*VZero**(2./3) + 42*c*VZero**(4./3) + 15*b*VZero**2))

                # Bulk modulus 2nd pressure derivative
                B2P =((4*VZero**(13./3)*(27*d*(7*c*VZero**(4./3) + 10*b*VZero**2) + VZero**(2./3)*(7*c*(5*b*VZero**2))))/
                      (27*d*VZero**(2./3) + 14*c*VZero**(4./3) + 5*b*VZero**2)**3)
                # Convert to 1/GPa
                B2P = B2P/(chunit/10); 
            
                # Energy at zero pressure
                E0 = energyFunction(VZero)
            

            elif degree == 4:
                # this is the mechanical properties method for the
                # BM 5-parameter fit to energy. 
                try:
                    a = fitCoeff[4]
                    b = fitCoeff[3]
                    c = fitCoeff[2]
                    d = fitCoeff[1]
                    e = fitCoeff[0]
                except IndexError as err:
                    print("Error: expected 5 coefficients, given {}".format(len(fitCoeff)))
                    raise err
                # the following are analytic equations for a polynomial
                # of the for E = a + bV^(-2/3)+cV^(-4/3)+dV^(-2)+eV^(-8/3)
                P=(8*e)/(3*VZero**(11./3)) + (2*d)/VZero**3 + (4*c)/(3*VZero**(7./3)) + (2*b)/(3*VZero**(5./3))
                P=P*chunit

                # Bulk Modulues.
                B =(2*(44*e + 27*d*VZero**(2./3) + 14*c*VZero**(4./3) + 5*b*VZero**2))/(9*VZero**(11./3))
                # chunit converts to kBar, the chunit/10 converts to GPa
                B = B*(chunit/10)

                # Bulk Modulus Pressure Derivative. 
                BP=((484*e + 243*d*VZero**(2./3) + 98*c*VZero**(4./3) + 25*b*VZero**2)/
                    (132*e + 81*d*VZero**(2./3) + 42*c*VZero**(4./3) + 15*b*VZero**2))

                # Bulk Modulus 2nd Pressure Derivative
                B2P =((4*VZero**(13./3)*(27*d*(22*e + 7*c*VZero**(4./3) + 10*b*VZero**2) + VZero**(2./3)*(990*b*e*VZero**(2./3) + 7*c*(176*e + 5*b*VZero**2))))/
                      (44*e + 27*d*VZero**(2./3) + 14*c*VZero**(4./3) + 5*b*VZero**2)**3)
                # convert to units of 1/GPa
                B2P = B2P/(chunit/10)

                # Energy at zero pressure
                E0 = energyFunction(VZero)
        else:
            print("*"*60)
            print("Error: Strain type note supported in fitCoeffToMechanicalProperties function from EOSUtil.py\n\n")

                
    
    res=[VZero, E0, P, B, BP, B2P]
    return res
    
                                     

def energyFit(energies,volumes,strain,degree,fullOut=False):
    # inputs:
    #       a list of energies
    #       a list of volumes
    #       a function to calculate a strain by.
    #       a degree for the fitted polynomial
    strains = [strain(volume) for volume in volumes]
    fitOut = polyfit(array(strains),array(energies),degree,full=True)
    coeff = fitOut[0]
    res = fitOut[1]
    poly = poly1d(coeff)
    
    if fullOut:
        return [poly,res,coeff]
    else:
        return poly

def hybridFit(energies,pressures,volumes,strain,strainDeriv,degree,energyWeight=None,fullOut=False):
    """hybridFit(energies,pressures,volumes,strain,strainDeriv,
                 degree,energyWeight=None,fullOut=False) 
    is a function that takes energy, pressure and volume data, 
    and fits a polynomial of a given degree using a specified method.
     
    input:
       energies        : an array containing the pressures to fit to
       pressures       : an array containing the pressures to fit to
       volumes         : an array containing the volumes corresponding to the energies 
       strain          : a function that calculates strain for the chosen fit method
       strainDeriv     : a function that calculates strain's derivative wrt Volume
       degree          : the degree of the polynomial used for fitting
       fullOut         : fullOut decides if the polynomial, residual, AND coeff
                         list will be returned, or just the polynomial
       method          : a string to decide the method (e.g. "BM", "mBM", 'log')
    
    outputs:
       if fullOut==True:
         return [polynomial,residual,coefficient list]
       else:
         return  
         
       polynomial is a poly1d object that gives energy as a function of strain
       residual is the sum of squared error
       coefficient list is the list of fit coefficients
     
    For more info, see EOSUtil.py"""

    chunit=1602.189;
    pressRange = max(pressures)-min(pressures);
    energyRange = max(energies)-min(energies);

    if energyWeight==None:
        En_weight = (1/energyRange)/(1/energyRange+1/pressRange);
        Pr_weight = (1/pressRange)/(1/energyRange+1/pressRange);
    else:
        En_weight = (energyWeight)/energyRange;
        Pr_weight = (1-energyWeight)/pressRange;

    #print("prWeight: {} enWeight: {}".format(En_weight,Pr_weight))
    
    #set up the linear system to be solved in least squares
    EnergyA=NormalEquationBuild(volumes,strain,degree)
    PressA=-chunit*NormalEquationBuild(volumes,strain,degree-1,xMod=strainDeriv,iMod=lambda x: x)
    PressA=horzcat[PressA,zeros([PressA.shape[0],1])]


    A = vertcat[En_weight*EnergyA,Pr_weight*PressA]
    

    # Set up the observed y value matrix
    energies = matrix(energies).transpose()
    pressures = matrix(pressures).transpose()
    #print("energies: \n{}\n {}".format(energies,type(energies)))
    #print("pressures: \n{}\n {}".format(pressures,type(energies)))
    y = vertcat[En_weight*energies,Pr_weight*pressures]
    #print(A)
    #print(y)
  
    hybridCoeff=pinv(A)*y
    
    #construct the energy polynomial
    #hybridCoeff = array(flipud(hybridCoeff).transpose())[0]
    hybridCoeff = array(hybridCoeff.transpose())[0]
    # print("hybridCoeff:\n{}".format(hybridCoeff))
    energyPoly = poly1d(hybridCoeff)

    # for i in range(len(volumes)):
    #     print("E(fit):{} E(obs):{} E(nst):{} err:{}".format(energyPoly(strain(volumes[i])),energies[i],energyPoly(volumes[i]),energies[i]-energyPoly(strain(volumes[i]))))

    
    return energyPoly

def NormalEquationBuild(xData,strain,degree,xMod=None,iMod=None):
    """ NormalEquationBuild(xData,strain,degree,modification)
    
    NormalEquationBuild is a function that takes in observed points, and
     a function to evaulate of them. Then, it creates powers of that
     function from 0 to degree, and then adds it to the A matrix in the
     A^tAb=A^ty formulation of least squares.
    
    This function can also accepts two 'modification' function, which are
     useful when you try to fit a function and its derivative at the same
     time.
     
    xMod is a function that modifies xData based on xData's value
    iMod is a function that modifies xData based on the column of the 
     normal equation being built
     """
    if(xMod==None):
        xMod= lambda x: 1

    if(iMod==None):
        iMod=lambda x: 1
            
    A = zeros([len(xData),degree+1])
    for i in range(len(xData)):
        #initialize the new row
        temp=zeros([1,degree+1])
        #calculate the value of the basis function
        basisValue = strain(xData[i])
        for j in range(degree+1):
            # calculate the power of the basis function to be used in
            # fitting
            temp[0][j]= basisValue**(j)*iMod(j+1)
        # assign the created row of the A matrix.
        # the row is flipped, because matlab arranges its coefficients high
        # to low order, rather thn low to high.
        A[i,:] = fliplr(temp)*xMod(xData[i])

    return A


class strainFactory():
    # strainFactory is a class that exists to create anonymous strain functions
    # it can create these functions for a number of different strain types
    # essentially, all strains are eulerian, lagrangian, or hencky strains
    # other names for these types are also included
    supportedStrains = ['euler','birch-murnaghan','murnaghan','bm','lagrange','hencky','log','logarithmic']
    
    def __init__(self,strainType):
        # define a default strain in init's strainType fails
        self.setStrain('birch-murnaghan')
        # define strain using the init's strainType
        self.setStrain(strainType)

    def setStrain(self,strainType):
        # set the factory's strain type and strain function according to input
        
        # Families are defined in EOSUtil.py. Check if the input strain type is
        # in any of the families
        if   strainType.lower() in eulerianFamily:
            self.strainType=strainType
            self.strain = lambda vol: (vol)**(-2./3.)
            self.strainDeriv = lambda vol: (-2./3.)*(vol)**(-5./3.)

        elif strainType.lower() in lagrangianFamily:
            self.strainType=strainType
            self.strain = lambda vol: (vol)**(2./3.)
            self.strainDeriv = lambda vol: (2./3.)*(vol)**(-1./3.)

        elif strainType.lower() in logarithmicFamily:
            self.strainType=strainType
            self.strain = lambda vol: log(vol)
            self.strainDeriv = lambda vol: 1/vol

        else:
            # couldn't find the family for the input strainType, so 
            # print some messages
            print("Error: supported strains include:")
            for family in strainFamilies:
                for strain in family:
                    print("  ",strain)
            print("received {}".format(repr(strainType)))
            print("Strain type will remain: {}.\nStrain type is case insensitive".format(self.strainType))

    def getStrain(self): 
        # return an anonymous function that calculates strain
        # see "setStrain" for the definition of the functions
        return self.strain
    def getStrainType(self): 
        # return the string for the string type
        return self.stringType

    def getStrainDeriv(self):
        # return an anonymous function that calculates strain's
        # first derivative
        # see "setStrain" for the definition of the functions
        return self.strainDeriv
        

