

class debyeTempFactory():
    """
     The debyeTempFactory class takes in parameters of some sort, and returns 
      a function that calculates the debye temperature. For any alternate
      definition of the debye temperature, this function can/should be extended
      to comply with that model 
    """
    def getDebyeTempOfVol():
        # an example of a bad debye temperature function
        return lambda volume: 300
