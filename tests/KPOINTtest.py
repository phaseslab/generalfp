#!/usr/bin/env python
# INCARDemo.py showcases features of ClassINCAR as of 2014/05/13

from sys import argv as arguments
import sys
# set up executable path, name, and the library path
executablePath= arguments[0]
executableName = executablePath.split("/")[-1]
libPath = executablePath.split("/")[:-2]
libPath.extend(['lib',''])
libPath = "/".join(libPath)

print "libPath:",libPath,"\n\n"
sys.path.append(libPath)
from FPClasses import ClassKPOINTS

from copy import deepcopy


#Create a new INCAR by giving it a name in current working directory
baseKPOINTS  = ClassKPOINTS("KPOINTS")

# show the INCAR
print "\nbase:\n",baseKPOINTS,"\n\n"

# make updated versions according to INCAR.low and INCAR.high
lowKPOINTS  = baseKPOINTS.update("KPOINTS.low")
highKPOINTS = baseKPOINTS.update("KPOINTS.high")


# show the updated INCARS, and the unchanged original INCAR
print "\nbase updated:\n"
print "\nlowINCAR:\n",lowKPOINTS,"\n\n"
print "\noriginal:\n",baseKPOINTS,"\n\n"
print "\nhighINCAR:\n",highKPOINTS,"\n\n"
