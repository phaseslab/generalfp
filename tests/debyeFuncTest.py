#!/usr/bin/env python

import sys

arguments = sys.argv
# set up executable path, name, and the library path
executablePath= arguments[0]
executableName = executablePath.split("/")[-1]
libPath = executablePath.split("/")[:-2]
libPath.extend(['lib',''])
libPath = "/".join(libPath)
# add the generalFP library path to sys.path
sys.path.append(libPath)

from debyeThird import debyeFunc, debyeCV
from numpy import arange
import matplotlib.pyplot as plt

TDebye = 300
TMin   = 0
TMax   = 1200
TStep  = 10

TData   = arange(TMin,TMax,TStep)


# dividing by float(T) is important, to avoid integer division
ratio  = lambda T: TDebye/float(T) if T>0 else float("inf")

debyeU = lambda T: 3*T*debyeFunc(ratio(T))
C_V    = lambda T: debyeCV(ratio(T))


UData   = [debyeU(el) for el in TData]
CVData  = [C_V(el) for el in TData]

plt.figure(1)
plt.plot(TData, UData,'r-',label='Unitless Debye Internal Energy')
plt.plot(TData,[1000*el for el in CVData],'b-',label='Unitless Debye Internal Energy')

plt.show(block=False)










plt.show()
