#!/usr/bin/env python

#####################################################################
#
#Import necessary libraries and functions

from numpy import polyfit,array,linspace,poly1d

#from matplotlib import pyplot
import os, sys

arguments = sys.argv
# set up executable path, name, and the library path
executablePath= arguments[0]
executableName = executablePath.split("/")[-1]
libPath = executablePath.split("/")[:-2]
libPath.extend(['lib',''])
libPath = "/".join(libPath)
# add the generalFP library path to sys.path
sys.path.append(libPath)
from FPUtil  import energyFromPath, volumeFromPath, pressureFromPath
from EOSUtil import strainFactory, energyFit, hybridFit,chunit
from EOSUtil import mechanicalPropertiesFromFitCoeff

#####################################################################
#
# Process configuration options

# Add some default options
shouldPlot = False
ignoreList = []
strainType = 'BM'
degree = 4
config = 'EOSconfig'


# process options from command line
for arg in sys.argv[1:]:
    #check if any of the config options are in the command line
    #set the appropriate config option if they are
    if 'plot' in arg.lower():
        shouldPlot = (arg.split('=')[1].lower()=='true')
    elif 'ignore' in arg.lower():
        ignoreList = [int(el) for el in (arg.split('=')[1]).split(',')]
    elif 'strain' in arg.lower():
        strainType = arg.split('=')[1]
    elif 'degree' in arg.lower():
        degree = int(arg.split('=')[1])
    elif 'config' in arg.lower():
        config = arg.split('=')[1]
    else:
        print('unsupported argument\: {1}'.format(arg))

# check options from config file, if it exists
if os.path.isfile(config):
    f = open(config,'r')
    configLines = [el.strip() for el in f.readlines()]
    for arg in configLines:
    #check if any of the config options are in the config file
    #set the appropriate config option if they are
        if 'plot' in arg.lower():
            shouldPlot = (arg.split('=')[1].lower()=='true')
        elif 'ignore' in arg.lower():
            ignoreList = [int(el) for el in (arg.split('=')[1]).split(',')]
        elif 'strain' in arg.lower():
            strainType = arg.split('=')[1]
        elif 'degree' in arg.lower():
            degree = int(arg.split('=')[1])
        elif 'config' in arg.lower():
            config = arg.split('=')[1]
        else:
            print("unsupported argument:\t{}".format(repr(arg)))

#set strain to be the appropriate strain function based on configuration input        
strain      = strainFactory(strainType).getStrain()
strainDeriv = strainFactory(strainType).getStrainDeriv()

#####################################################################
# Configuration complete
# Acquiring data

# find which folders in this directory might have data to plot
volFolders = [folder for folder in os.listdir('.') if (('vol_' in folder) and os.path.isdir(folder))]

# find the indices from the volume list that will be used
useList = [el for el in range(len(volFolders)) if not(el in ignoreList)]

# get the energies and volumes at those paths
energies = [energyFromPath(folder) for folder in volFolders]
volumes = [volumeFromPath(folder) for folder in volFolders]
pressures = [pressureFromPath(folder) for folder in volFolders]

# remember the ignored energies
ignoredEnergies   = [energies[i] for i in ignoreList]
ignoredVolumes    = [volumes[i] for i in ignoreList]
ignoredPressures  = [pressures[i] for i in ignoreList]

# assemble the used energies, and calculate their strains
usedEnergies  = [energies[i] for i in useList]
usedVolumes   = [volumes[i] for i in useList]
usedPressures = [pressures[i] for i in useList]
usedStrains   = [strain(volume) for volume in usedVolumes]

# construct the fit of energy as a function of strain
energyFit_EnergyPoly = hybridFit(usedEnergies,
                                 usedPressures,
                                 usedVolumes,
                                 strain,
                                 strainDeriv,
                                 degree,
                                 energyWeight=1)
enCoeff = energyFit_EnergyPoly.c


pressureFit_EnergyPoly = hybridFit(usedEnergies,
                                 usedPressures,
                                 usedVolumes,
                                 strain,
                                 strainDeriv,
                                 degree,
                                 energyWeight=1e-6)
prCoeff = pressureFit_EnergyPoly.c
# pressureFit_PressurePoly,prRes,prCoeff = pressureFit(usedPressures,
#                                                      usedVolumes,
#                                                      strain,
#                                                      degree,
#                                                      fullOut=True)


hybridFit_EnergyPoly = hybridFit(usedEnergies,
                                 usedPressures,
                                 usedVolumes,
                                 strain,
                                 strainDeriv,
                                 degree)
hyCoeff = hybridFit_EnergyPoly.c

# use the anlaytical derivative energyFit_EnergyPoly gets from its
# its class to find the energy fit's pressure polynomial.
energyFit_Energy     = lambda vol: energyFit_EnergyPoly(strain(vol))
energyFit_Pressure   = lambda vol: (-chunit*strainDeriv(vol)*
                                       energyFit_EnergyPoly.deriv()(strain(vol)))

pressureFit_Energy     = lambda vol: pressureFit_EnergyPoly(strain(vol))
pressureFit_Pressure   = lambda vol: (-chunit*strainDeriv(vol)*
                                       pressureFit_EnergyPoly.deriv()(strain(vol)))

hybridFit_Energy     = lambda vol: hybridFit_EnergyPoly(strain(vol))
hybridFit_Pressure   = lambda vol: (-chunit*strainDeriv(vol)*
                                       hybridFit_EnergyPoly.deriv()(strain(vol)))

# print("Fit Propertes:")
# print("energy residual  \t=\t{}\npressure residual\t=\t{}".format(enRes[0]/(sum(map(lambda x: x**2,usedEnergies))),
#                                                  prRes[0]/(sum(map(lambda x: x**2,usedPressures)))))

# evaluate the fitted polynomial 
minVol = min(volumes)
maxVol = max(volumes)
fitVols = linspace(minVol*.99,maxVol*1.01,num=60)

energyFit_Energies = [energyFit_Energy(vol) for vol in fitVols]
energyFit_Pressures = [energyFit_Pressure(vol) for vol in fitVols]

pressureFit_Energies = [pressureFit_Energy(vol) for vol in fitVols]
pressureFit_Pressures = [pressureFit_Pressure(vol) for vol in fitVols]

hybridFit_Energies = [hybridFit_Energy(vol) for vol in fitVols]
hybridFit_Pressures = [hybridFit_Pressure(vol) for vol in fitVols]


#####################################################################
#
# Plot figures if necessary

if shouldPlot:
    # we only need matplotlib if we're going to plot
    import matplotlib.pyplot as plt


    plt.figure(1)
    # put used and ignored vasp outputs onto the chart
    plt.plot(usedVolumes,usedEnergies,'ko',label='VASP output')
    if ignoredVolumes!=[]:
        plt.plot(ignoredVolumes,ignoredEnergies,'bo',label='VASP output: ignored')
    # add the fit curve to the plot
    plt.plot(fitVols, energyFit_Energies,'r-',
             label='{} EOS, order {} energy Fit'.format(strainType,degree))
    plt.plot(fitVols, hybridFit_Energies,'g-',
             label='{} EOS, order {} hybrid fit'.format(strainType,degree))
    plt.plot(fitVols, pressureFit_Energies,'b-',
             label='{} EOS, order {} pressure fit'.format(strainType,degree))

    # create the legend
    plt.legend(bbox_to_anchor=(0,0,1,1),bbox_transform=plt.gcf().transFigure)
    # show the plot
    plt.show(block=False)

    
    plt.figure(2)
    # put used and ignored vasp outputs onto the chart
    plt.plot(usedVolumes,usedPressures,'ko',label='VASP output')
    if ignoredVolumes!=[]:
        plt.plot(ignoredVolumes,ignoredPressures,'bo',label='VASP output: ignored')
        
    # add the fit curves to the plot
    plt.plot(fitVols,energyFit_Pressures,'r-',
             label='{} EOS, order {} from energy fit'.format(strainType,degree))
    plt.plot(fitVols,hybridFit_Pressures,'g-',
             label='{} EOS, order {} from hybrid fit'.format(strainType,degree))
    plt.plot(fitVols,pressureFit_Pressures,'b-',
             label='{} EOS, order {} direct pressure fit'.format(strainType,degree))
    # create the legend
    plt.legend(bbox_to_anchor=(0,0,1,1),bbox_transform=plt.gcf().transFigure)
    # show the plot
    plt.show(block=False)

# mechanicalPropertiesFromFitCoeff is a function that gets 
# V0,B0,B', and B" from the fitted coefficients
try:
    energyProperties = mechanicalPropertiesFromFitCoeff(enCoeff,
                                                        strainType,
                                                        degree,
                                                        (minVol+maxVol)/2)
    print("Energy Fit Mechanical Properties:")
    print("VZero\t=\t{}".format(energyProperties[0]))
    print("E_0\t=\t{}".format(energyProperties[1]))
    print("B\t=\t{}".format(energyProperties[3]))
    print("B'\t=\t{}".format(energyProperties[4]))
    print("B\"\t=\t{}".format(energyProperties[5]))
except RuntimeError as e:
    print("Error getting mechanical properties for energy fit")
    print(e)

try:
    pressureProperties = mechanicalPropertiesFromFitCoeff(prCoeff,
                                                          strainType,
                                                          degree,
                                                          (minVol+maxVol)/2)
    print("Pressure Fit Mechanical Properties:")
    print("VZero\t=\t{}".format(pressureProperties[0]))
    print("E_0\t=\t{}".format(pressureProperties[1]))
    print("B\t=\t{}".format(pressureProperties[3]))
    print("B'\t=\t{}".format(pressureProperties[4]))
    print("B\"\t=\t{}".format(pressureProperties[5]))
except RuntimeError as e:
    print("Error getting mechanical properties for pressure fit")
    print(e)

try:
    hybridProperties = mechanicalPropertiesFromFitCoeff(hyCoeff,
                                                        strainType,
                                                        degree,
                                                        (minVol+maxVol)/2)
    print("Hybrid Fit Mechanical Properties:")
    print("VZero\t=\t{}".format(hybridProperties[0]))
    print("E_0\t=\t{}".format(hybridProperties[1]))
    print("B\t=\t{}".format(hybridProperties[3]))
    print("B'\t=\t{}".format(hybridProperties[4]))
    print("B\"\t=\t{}".format(hybridProperties[5]))
except RuntimeError as e:
    print("Error getting mechanical properties for energy fit")
    print(e)

if shouldPlot:
    #this show keeps the plots open until the user closes them or the
    # program is closed
    plt.show()
