#!/usr/bin/env python
from sys import argv as arguments
import sys

# set up executable path, name, and the library path
executablePath= arguments[0]
executableName = executablePath.split("/")[-1]
libPath = executablePath.split("/")[:-2]
libPath.extend(['lib',''])
libPath = "/".join(libPath)


sys.path.append(libPath)
from FPClasses import *

print arguments 
#Load in default INCAR, KPOINT, pbs file, POTCAR, and POSCAR
