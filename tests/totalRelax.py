#!/usr/bin/env python

# ********************************************************************
# importing modules
from datetime import datetime
from time import sleep
from os import listdir
from copy import deepcopy
from sys import argv as arguments
from time import sleep as sleep
import sys

# set up executable path, name, and the library path
executablePath= arguments[0]
executableName = executablePath.split("/")[-1]
libPath = executablePath.split("/")[:-2]
libPath.extend(['lib',''])
libPath = "/".join(libPath)

# add the generalFP library path to sys.path
sys.path.append(libPath)
from FPClasses import *

# ********************************************************************
# general configuration settings
maxRelaxations = 10   # the largest number of relaxations to allow
local = False         # 
sleepTime = 20        # sleep time in seconds

# ********************************************************************
# setting up the base file

# find the PBS file
directoryContents = os.listdir(".")
pbsFiles     = [el for el in directoryContents if 'pbs'     in el]
basePBSString = ''
for el in pbsFiles: 
    if el.endswith('pbs'): 
        basePBSString=el

# Set the base files   
baseINCAR   = ClassINCAR("INCAR")
baseKPOINTS = ClassKPOINTS("KPOINTS")
basePOTCAR  = ClassPOTCAR("POTCAR")
basePOSCAR  = ClassPOSCAR("POSCAR")
basePBS     = ClassPBSScript(basePBSString)
baseVASPjob = VASPjob(".",
                      baseINCAR,
                      basePOSCAR,
                      baseKPOINTS,
                      basePOTCAR,
                      basePBS)

# ********************************************************************
# allrelax
# copy the base job
job = baseVASPjob

# ********************************************************************
# Beginning to actually run relaxation jobs
print "*"*60
print "Beginning relaxations:\n"
for i in range(maxRelaxations):
    # set up the job
    job.setup()

    # run the job locally or through a queue
    if local == True:
        job.localRun()
    elif local == False:
        job.run()
    else:
        print "Error: in totalRelax, expected local=True, or local=False. Received local={}. edit the 'local' configuration option in totalRelax.py"
        raise Exception

    # wait for the job to finish
    while not(job.isFinished()):
        sleep(20)
        print "waiting for job to finish. Current time:", datetime.today()

    # check on the CONTCAR and the POSCAR now that the job has finished
    try:
        newCONTCAR = ClassPOSCAR(os.path.join(job.getPath(),"CONTCAR"))
        newPOSCAR = ClassPOSCAR(os.path.join(job.getPath(),"POSCAR"))
    
        if newCONTCAR==newPOSCAR:
            print "CONTCAR == POSCAR at iteration {}".format(i)
            break
        else:
            job.POSCAR = newCONTCAR
    except IOError as e:
        print "IOError attempting to open CONTCAR and POSCAR. Did Vasp succeed?"
        raise e

print "Relaxations complete after {} iterations".format(i)
