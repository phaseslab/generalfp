#!/usr/bin/env python
# INCARDemo.py showcases features of ClassINCAR as of 2014/05/13

from sys import argv as arguments
import sys
# set up executable path, name, and the library path
executablePath= arguments[0]
executableName = executablePath.split("/")[-1]
libPath = executablePath.split("/")[:-2]
libPath.extend(['lib',''])
libPath = "/".join(libPath)

print "libPath:",libPath,"\n\n"
sys.path.append(libPath)
from FPClasses import ClassINCAR

from copy import deepcopy





#Create a new INCAR by giving it a name in current working directory
baseINCAR  = ClassINCAR("INCAR")

# show the INCAR
print "\nbase:\n",baseINCAR,"\n\n"

# make updated versions according to INCAR.low and INCAR.high
lowINCAR = baseINCAR.update("INCAR.low")
highINCAR = baseINCAR.update("INCAR.high")


# show the updated INCARS, and the unchanged original INCAR
print "\nbase updated:\n"
print "\nlowINCAR:\n",lowINCAR,"\n\n"
print "\noriginal:\n",baseINCAR,"\n\n"
print "\nhighINCAR:\n",highINCAR,"\n\n"
