#!/usr/bin/env python

# ********************************************************************
# importing modules
from time import sleep
from os import listdir
from copy import deepcopy
from sys import argv as arguments
from time import sleep as sleep
import sys

# set up executable path, name, and the library path
executablePath= arguments[0]
executableName = executablePath.split("/")[-1]
libPath = executablePath.split("/")[:-2]
libPath.extend(['lib',''])
libPath = "/".join(libPath)

# add the generalFP library path to sys.path
sys.path.append(libPath)
from FPClasses import *
from FPUtil import *

# ********************************************************************
# general configuration settings
nVol = 9           # The number of volumes in the EV curve
local = False      # Decides whether jobs are run locally or not
sleepTime = 20     # The amount of time between checkups on running jobs
config= 'EVConfig' # The default name for a config file

#process options from command line
for arg in sys.argv[1:]:
    #check if any of the config options are in the command line
    #set the appropriate config option if they are
    if 'nvol' in arg.lower():
        nVol = arg.split('=')[1]
    elif 'local' in arg.lower():
        local = arg.split('=')[1].lower()=='true'
    elif 'sleeptime' in arg.lower():
        sleepTime = arg.split('=')[1]
    elif 'config' in arg.lower():
        config = arg.split('=')[1]
    else:
        print('unsupported argument\: {1}'.format(arg))

# check options from config file, if it exists
if os.path.isfile(config):
    f = open(config,'r')
    configLines = [el.strip() for el in f.readlines()]
    for arg in configLines:
    #check if any of the config options are in the config file
    #set the appropriate config option if they are
        if 'nvol' in arg.lower():
            nVol = arg.split('=')[1]
        elif 'local' in arg.lower():
            local = arg.split('=')[1].lower()=='true'
        elif 'sleeptime' in arg.lower():
            sleepTime = arg.split('=')[1]
        else:
            print('unsupported argument\: {1}'.format(arg))
        

# ********************************************************************
# setting up the base file

# find the PBS file
directoryContents = os.listdir(".")
pbsFiles     = [el for el in directoryContents if 'pbs'     in el]
basePBSString = ''
for el in pbsFiles: 
    if el.endswith('pbs'): 
        basePBSString=el

# Set the base files   
baseINCAR   = ClassINCAR("INCAR")
baseKPOINTS = ClassKPOINTS("KPOINTS")
basePOTCAR  = ClassPOTCAR("POTCAR")
basePOSCAR  = ClassPOSCAR("POSCAR")
basePBS     = ClassPBSScript(basePBSString)
baseVASPjob = VASPjob(".",
                      baseINCAR,
                      basePOSCAR,
                      baseKPOINTS,
                      basePOTCAR,
                      basePBS)

jobList = []

# ********************************************************************
# volumerelax

# copy the base job as a starting point
volumeRelaxVASPjob = deepcopy(baseVASPjob)

# we'd like to run the volume relaxation in its own folder, so
# change path to the volumeRelax folder
volumeRelaxVASPjob.setPath("volumeRelax")

# update the ISIF setting to 7, which only relaxes volumes
volumeRelaxVASPjob.INCAR.setFlags({'ISIF':'7'})

# add the job to the job list if setup
tendJobList([volumeRelaxVASPjob],local=local,waitTime=sleepTime)


baseVASPjob.POSCAR = ClassPOSCAR("volumeRelax/CONTCAR")


# ********************************************************************
# cellrelax
# copy the base job
cellRelaxVASPjob = deepcopy(baseVASPjob)
# change the path to the cellRelax folder
cellRelaxVASPjob.setPath(os.path.join('.',"cellRelax"))
# update the ISIF setting to 5
cellRelaxVASPjob.INCAR.setFlags({'ISIF':'6'})


jobList.append(cellRelaxVASPjob)


# ********************************************************************
# atomrelax
# copy the base job
atomRelaxVASPjob = deepcopy(baseVASPjob)
# change the path to the atomRelax folder
atomRelaxVASPjob.setPath(os.path.join('.',"atomRelax"))
# update the ISIF setting to 5
atomRelaxVASPjob.INCAR.setFlags({'ISIF':'2'})


jobList.append(atomRelaxVASPjob)


# ********************************************************************
# allrelax
# copy the base job
allRelaxVASPjob = deepcopy(baseVASPjob)
# change the path to the allRelax folder
allRelaxVASPjob.setPath(os.path.join('.',"allRelax"))
# update the ISIF setting to 5
allRelaxVASPjob.INCAR.setFlags({'ISIF':'3'})

jobList.append(allRelaxVASPjob)


# ********************************************************************
# Beginning to actually run relaxation jobs
print("*"*60)
print("\nBeginning relaxations:\n")


tendJobList(jobList,local=True)


# Check through all the jobs, taking the "most relaxed" version
# also, set 'EVDict' to have an appropriate ISIF setting for the
# relaxation that converged and preserved symmetry
# At present, there is no symmetry check
if allRelaxVASPjob.isConverged():
    EVBaseJob=allRelaxVASPjob
    lowPOSCAR = ClassPOSCAR(os.path.join(allRelaxVASPjob.getPath(),"CONTCAR"))
    EVDict = {'ISIF':'4'}
elif cellRelaxVASPjob.isConverged():
    EVBaseJob=cellRelaxVASPjob
    lowPOSCAR = ClassPOSCAR(os.path.join(cellRelaxVASPjob.getPath(),"CONTCAR"))
    EVDict = {'ISIF':'5'}
elif atomRelaxVASPjob.isConverged():
    EVBaseJob=atomRelaxVASPjob
    lowPOSCAR = ClassPOSCAR(os.path.join(atomRelaxVASPjob.getPath(),"CONTCAR"))
    EVDict = {'ISIF':'2'}
elif volumeRelaxVASPjob.isConverged():
    EVBaseJob=volumeRelaxVASPjob
    lowPOSCAR = ClassPOSCAR(os.path.join(volumeRelaxVASPjob.getPath(),"CONTCAR"))
    EVDict = {'ISIF':'7','NSW':'0'}
else:
    print("no relaxation jobs appear to have converged")


print("*"*60)
print(" Beginning low accuracy EV curve with relaxation:\n")
# low EV curve with the highest symmetry preserving relaxation
lowBaseJob = deepcopy(EVBaseJob)
lowBaseJob.INCAR=lowBaseJob.INCAR.update("INCAR.low")
lowBaseJob.INCAR.setFlags(EVDict)
lowBaseJob.KPOINTS=lowBaseJob.KPOINTS.update("KPOINTS.low")
lowBaseJob.POTCAR=lowBaseJob.POTCAR.update("POTCAR.low")
lowBaseJob.PBSScript=lowBaseJob.PBSScript.update(EVBaseJob.PBSScript.getName()+".low")
lowBaseJob.POSCAR=lowPOSCAR

lowJobList = []
runningJobs = [] 
# spacing evenly by 1 percent, make jobs for the EV curve
for percent in range(-nVol/2+1,nVol/2+1):
    # Copy the low accuracy base job
    evPoint = deepcopy(lowBaseJob)
    
    # rescale it
    baseScale = evPoint.POSCAR.getScale()
    evPoint.POSCAR.setScale(baseScale*(1+percent/100.))

    # set the point's path, so it doesn't overwrite others
    evPoint.setPath(os.path.join("lowAccuracyEV/","vol_"+str(evPoint.POSCAR.getScale())))
    lowJobList.append(evPoint)

tendJobList(lowJobList,local=True)


print("*"*60)
print(" Beginning high accuracy relaxations:\n")
# high EV curve with the highest symmetry preserving relaxation

highJobList = []
for job in lowJobList:
    # copy all the jobs from the low accuracy runs
    highJob = deepcopy(job)
    # update VASP inputs with .high suffix files, if they exist.
    highJob.INCAR=highJob.INCAR.update("INCAR.high")
    highJob.INCAR.setFlags(EVDict)
    highJob.KPOINTS=highJob.KPOINTS.update("KPOINTS.high")
    highJob.POTCAR=highJob.POTCAR.update("POTCAR.high")
    highJob.PBSScript=highJob.PBSScript.update(job.PBSScript.getName()+".high")
    # grab the CONTCAR from the old job
    highJob.POSCAR =ClassPOSCAR(os.path.join(job.getPath(),"CONTCAR"))
    highJob.setPath(os.path.join("highAccuracyEV/","vol_"+str(highJob.POSCAR.getScale())))
    # add the constructed job to the list of jobs to run
    highJobList.append(highJob)

tendJobList(highJobList,waitTime=sleepTime,local=True)

print("*"*60)
print(" Beginning static calculation:\n")
# static EV curve with the staticest symmetry preserving relaxation
staticBaseJob = deepcopy(EVBaseJob)
staticJobList = []

for job in highJobList:
    # copy all the jobs from the high accuracy runs
    staticJob = deepcopy(job)
    staticJob.INCAR.setFlags({'PREC':'HIGH',
                                  'ISMEAR':'-5',
                                  'IBRION':'-1',
                                  'NSW':'0',
                                  'ISIF':'2'})
    # update VASP inputs with .static suffix files, if they exist.
    staticJob.INCAR=staticJob.INCAR.update("INCAR.static")
    staticJob.INCAR.setFlags(EVDict)
    staticJob.KPOINTS=staticJob.KPOINTS.update("KPOINTS.static")
    staticJob.POTCAR=staticJob.POTCAR.update("POTCAR.static")
    staticJob.PBSScript=staticJob.PBSScript.update(job.PBSScript.getName()+".static")
    
    # grab the CONTCAR from the old job
    staticJob.POSCAR =ClassPOSCAR(os.path.join(job.getPath(),"CONTCAR"))
    staticJob.setPath(os.path.join("staticEV/","vol_"+str(staticJob.POSCAR.getScale())))
    staticJobList.append(staticJob)

#now that we have jobs
tendJobList(staticJobList,waitTime=sleepTime,local=True,jobType='static')

